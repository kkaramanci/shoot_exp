# INTRODUCTION #

This is the source code for Shoot - A framework, which uses a penalty shootout game for Cog Sci experiments

### Who is this for? ###

Experimenters in cog sci that are looking for alternatives for their experimental setup, for tasks such as 2AFC.
The experimental framework is provided within the easy-to-use OpenSesame program, so that simple setups may be run with little code modifications.
Well documented python code is provided for additional and advanced customizations

### How do I get set up? ###

* Dependencies -
Requires OpenSesame 3.0 or greater to run.
Download and install from http://osdoc.cogsci.nl/

* Setup

    Install and Run the OpenSesame Program

    Clone this repository

    Load up one of the available .opensesame files in this repository:

    shoot2afc_***      Shot choices to the left or right of the goalkeeper. 

    shoot_react_***    Press a button at the correct time to score

* Configuration

    In order to correctly load graphical assets, look for the "entities" code component loaded in OpenSesame
change the "path" variable to point to the assets folder at the location where this repository was cloned

       path = "*****your/repository/clone/address****/assets/"