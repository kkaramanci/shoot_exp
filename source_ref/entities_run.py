import pygame, math, random, scipy
from scipy.stats import norm as normaldist
from pygame.sprite import Sprite
from openexp.keyboard import keyboard

####### Entities_Run ##############################################################
# Declerations of experiment object classes 
# and settings/configurations.
# This code is run in the "run" loop of the experiment. 
# It occures before experiment presentation.
# Therefore does not hinder experiment runtime performance.
# 
# Object Classes declared:
# Target - Shows the location to where the shot is headed
# 
# Goal - The goal area and graphics
#
# Goalkeeper - The goalkeeper object, its motion properties and checking
# if goals are scored. Nested under the goal object
#
# Feedback - Presentation of textual feedback on the top side of the screen
#
# Ball - Ball object which travels towards target location at set velocity
#
# Vector - Generic object for vector calculations. Useful in motion.
#	
###### CONFIGURATION SETTINGS ###################################

	
# Path of the experiment graphics folder. Must include the final '/' for the adress. 
# Linux or Windows filepaths supported
path = "/Users/kkaramanci/Documents/Shoot/assets/"	

# RGB color codes for colors used in experiment for easy setup and reference
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
darkBlue = (0,0,128)
white = (255,255,255)
black = (0,0,0)    



###### EXPERIMENT PARAMETER SETTINGS ##############################################
# Settings dictionaries that translate worded settings to parameter values in code.
# Parameters may be altered to fit experiment or new ones may be defined
# But dictionaries should be used in setting up experiment sequences & trials 
# rather than writing direct in code to ensure proper referenced use throughout
# experiment and standardization over users of the experiment.
####################################################################################
DIFFICULTY = { 
	'easy' : 1,
	'medium' : 2,
	'hard' : 3
	}
SPEED = { 
	'slow' : 1,
	'medium' : 3,
	'fast' : 5
	}
	
SHOOT_SPEED = { 
	'slow' : 5,
	'medium' : 7,
	'fast' : 10
	}

##### Initialize global experiment variables###############
# Initialize the previous agency variable for the goalkeeper.
# This defines the level of control the subject has of scoring
# a goal in the previous trial
exp.set('prev_agency', -1)

#The time limit after which the experiment ends
exp.set('time_limit', 1200000) #20 mins

		
class Target(Sprite):
""" 
A target object that is displayed to indicate where a shot will land.
"""
	def __init__(self, position, *groups):
	"""
	Object is initialized at the 0,0 location and is hidden.
	"""
		Sprite.__init__(self, *groups)
		self.image = pygame.Surface([0, 0])
		self.image.set_colorkey(black)
		#self.image = pygame.Surface([20,20])
		#self.image.fill(blue)
		self.rect = self.image.get_rect()
		self.rect.x = position[0]
		self.rect.y = position[1]                

	def hide(self):
		self.image = pygame.Surface([0, 0])
		
	def show(self):
	"""
	Reveals the object at the current location as set in the object Sprite coordinates.
	"""
		self.image = pygame.image.load(path + "target.png").convert()
		self.image.set_colorkey(black)
		self.rect = self.image.get_rect()


	def update(self):
		pass
		
class Goal():
"""
This is the goal object which displays the goal graphics and serves as an anchor container for other objects.
Contains:
- A Target object: Target operates within the bounds of the goal rectangle
- A Goalkeeper object: Goalkeeper operates within the bounds of the goal rectangle

Arguments:
- position: x,y coordinates of the goals location on screen
- agency: Defines the agency of the subject. Does the subject have control over the shot outcome?
- deviation: Sets the deviation from the center of goal, of the contained goalkeeper object

Other Notes:
- rect: This is the Sprite argument that defines the rectangular bounds of the goal area


"""
	def __init__(self, position, agency, deviation, *groups):
	"""
	Initializes the Target and Goalkeeper objects at the relevant location and settings.
	Target is initialized hidden in the goal rectangle
	Goalkeeper is initialized with a set agency (whether subject has control on outcome), at the central x,y
	coordinates of the goal area with set deviation from the center of the goal
	"""
		self.rect = pygame.Rect(0,0,320,200)
		
		#self.image = pygame.Surface([0, 0])
		#self.image.set_colorkey(black)
		#self.image.fill(white)
		#self.rect = self.image.get_rect()
		
		self.rect.x = position[0]
		self.rect.y = position[1]
		
		self.agency = agency
		self.deviation = deviation
		
		self.target = Target(position, *groups)
		self.goalkeeper = Goalkeeper([self.rect.centerx,self.rect.centery], self.rect, self.agency, self.deviation, *groups)
		
	def paint_target(self, new_pos = ()):
	"""
	Reveals the target at new_pos or at a random location within the goal area if new_pos not supplied
	
	Arguments:
	- new_pos: x,y coordinates of the location to show the Target
	"""
		self.target.show()
		#If new_pos exists, set x coordinate to x coordinate of new_pos. y coordinate is random
		if new_pos:
			new_x = new_pos[0]
			new_y = self.rect.top + random.randrange(self.rect.height - self.target.rect.height)
		#If new_pos does not exist, set x,y to random positions in goal rect area
		else:
			new_x = self.rect.left + random.randrange(self.rect.width - self.target.rect.width)
			new_y = self.rect.top + random.randrange(self.rect.height - self.target.rect.height)
		self.target.rect.x = new_x
		self.target.rect.y = new_y

	
	def update(self):
		pass
	
	
class Feedback(Sprite):
	"""
	Feedback object displays text messages on the screen.
	"""
	def __init__(self, position, *groups):
	"""
	Initializes the text position and color
	
	Arguments:
	- position: (x,y) coordinates of position on screen
	"""
		Sprite.__init__(self, *groups)
		self.position = position        
		self.font = pygame.font.SysFont("monospace", 36, bold=True)
		self.image = pygame.Surface([0, 0])
		
		# initial color
		self.image.fill(black)
		self.rect = self.image.get_rect()
		# position and size of initial feedback
		self.rect.x = self.position[0]
		self.rect.y = self.position[1]  
		self.rect.width = self.image.get_width()
		self.rect.height = self.image.get_height()
	
	def showText(self, text, color):
	"""
	Shows feedback text
	Arguments:
	- text: the text message to display
	- color: text color to display
	"""
		self.image = self.font.render(text, 1, color)
		self.rect.width = self.image.get_width()
		self.rect.height = self.image.get_height()
		pass
		

	def hideText(self):
	"""
	Hides the current feedback message
	"""
		self.image = pygame.Surface([0, 0])
		pass
	
	def update(self):
		pass					

class Ball(Sprite):
	"""
	Ball object displays a ball on screen and handles the smooth motion of the ball
	"""
	def __init__(self, position, *groups):
	"""
	Initializes the ball position and initial velocity & destination/target to 0.
	Arguments:
	- position: initial position of the ball
	"""
		Sprite.__init__(self, *groups)
		self.velocity = 0
		# load the image of the ball
		self.image = pygame.image.load(path + "sball.png")
		self.rect = self.image.get_rect()
		# set the initial position of the ball
		self.rect.x = position[0]
		self.rect.y = position[1]
		# initial velocity set to 0
		self.velocity_vec = [0, 0]
		self.position_vec = [0, 0]
		self.position_init = [0, 0]
		self.position_vec[0] = position[0]
		self.position_vec[1] = position[1]
		self.position_init[0] = position[0]
		self.position_init[1] = position[1]
		# initial target destination for ball is empty
		self.target = None
		
	def update(self):
	"""
	Update function runs once per frame. At each frame a new position for the ball is calculated to simulate ball movement
	"""
		# Determine the direction from current position to the ball target
		self.dir = self.get_direction(self.target) 
		# before updating position check to see the ball is not at the target and has a velocity and direction
		if self.dir and self.velocity and not self.in_target(): 
			self.position_vec[0] += (self.dir[0] * self.velocity) # calculate velocity vector x component from direction and base velocity
			self.position_vec[1] += (self.dir[1] * self.velocity) # calculate velocity vector y component from direction and base velocity
			self.rect.center = (round(self.position_vec[0]),round(self.position_vec[1])) # move the center of the object for 1 time unit by the x,y vectors
		else:
			# if ball is not moving or at target/destination, reset velocity to 0 and target to None
			self.velocity = 0
			self.target = None
	
	
	def shoot(self, shoot_to = None):
	"""
	Ball is assigned a new target/destination and velocity
	"""
		if shoot_to:
			self.target = shoot_to
			self.velocity = configuration['shoot_speed']
	
	def in_target(self):
	"""
	Determines if the ball is at the target location. Returns True/False
	"""
		return self.target.contains(self.rect)
		
										  
										  
	def get_direction(self, target):
	"""
	Get the vector direction from object to target
	"""
		if self.target: 
			# create a vector from center x,y value for object
			position = Vector(self.rect.centerx, self.rect.centery) 
			# create a vector from center x,y value for target
			targetv = Vector(target.centerx, target.centery)
			
			# get total distance between target and position
			self.dist = targetv - position 
			# normalize
			direction = self.dist.normalize()
			return direction
			
	def reset(self):
	"""
	Reset the ball position, vector and target to init values
	"""
		self.velocity = 0
		self.velocity_vec = [0, 0]
		self.rect.x = self.position_init[0]
		self.rect.y = self.position_init[1]
		self.position_vec[0] = self.position_init[0]
		self.position_vec[1] = self.position_init[1]

		self.target = None
		

class Goalkeeper(Sprite):
	"""
	Goalkeeper object displays and moves the goalkeeper on screen.
	"""
	def __init__(self, position, goal_rect, agency, deviation, *groups):
	"""
	Initialize goalkeeper parameters
	Arguments:
	- Position: x,y coordinates of goalkeeper
	- goal_rect: the goal rectangular area of tended goal
	- agency: Control subject has over shot outcome (the goalkeepers ability to save)
	- deviation: deviation of position from center of goal
	"""
		Sprite.__init__(self, *groups)
		# load goalkeeper image
		self.image = pygame.image.load(path + "gk2.png")
		self.rect = self.image.get_rect()
		
		#position goalkeeper in goal area
		self.rect.centerx = position[0] 
		self.rect.centery = position[1] + 30
		self.goal_rect = goal_rect
		self.velocity_vec = [0, 0]
		self.position_vec = [0, 0]
		self.position_init = [0, 0]
		self.position_vec[0] = position[0]
		self.position_vec[1] = position[1]
		self.position_init[0] = position[0]
		self.position_init[1] = position[1]
		self.target = None
		self.saving = False
		
		# randomly decide left or right deviation and alter center position by that amount
		self.center = random.choice([self.goal_rect.centerx - (self.goal_rect.width / deviation), self.goal_rect.centerx + (self.goal_rect.width / deviation)])
		#generate a normally distributed random variable with mean at deviated center and std at 1/4 the width of goal
		self.norm = normaldist(self.center, self.goal_rect.width/4)
		# pick first target location from the normally distributed random variable random variable
		self.target = pygame.Rect(self.norm.rvs(1)[0], self.rect.centery,5,5)
		# set initial velocity for goalkeeper
		self.velocity = 5
		
		
	def update(self):
	"""
	At every frame, update the goalkeepers position depending on target location and velocity
	"""
		# before a shot is taken, this section enables the goalkeeper to wander
		# according to a normal distribution
		if not self.saving:
			# get direction from current position to target location
			self.dir = self.get_direction(self.target) 
			# check if velocity exists and not arrived at target
			if self.dir and self.velocity and not self.in_target():
				#set direction of horizontal velocity
				self.velocity = math.copysign(self.velocity, self.dir[0])
				# update new horizontal position by moving 1 time unit in direction at velocity
				self.position_vec[0] += (self.velocity)
				self.rect.centerx = round(self.position_vec[0])
		
				# if goalkeeper arrives at or passes by target position, pick new location
				# from normally distributed random variable
				if self.velocity < 0 and self.target.left > self.rect.left:
					self.target.x = self.norm.rvs(1)[0]
				if self.velocity > 0 and self.target.right < self.rect.right:
					self.target.x = self.norm.rvs(1)[0]	
			
			else:
				self.target.x = self.norm.rvs(1)[0]
		
		# after the shot, if the goalkeeper will save the shot, he moves towards the target location of the ball for the save
		# if he doesn't save, he stands still
		else:
			self.dir = self.get_direction(self.target) 
			if self.dir and self.velocity and not self.in_target(): 
			# calculate speed from direction to move and speed constant
				self.position_vec[0] += (self.dir[0] * self.velocity) 
				self.rect.centerx = round(self.position_vec[0])
			else:
				self.velocity = 0
				self.target = None

	def move(self, move_to = None):
	"""
	Assigns a target location for the goalkeeper to move to and sets motion velocity
	"""
		if move_to:
			self.save = True
			self.target = move_to
			self.velocity = 10
		

	def get_direction(self, target):
	"""
	Get the vector direction from object to target
	"""
		if self.target: 
			# create a vector from center x,y value for object
			position = Vector(self.rect.centerx, self.rect.centery) 
			# create a vector from center x,y value for target
			targetv = Vector(target.centerx, target.centery)
			
			# get total distance between target and position
			self.dist = targetv - position 
			# normalize
			direction = self.dist.normalize()
			return direction
			
			
	def in_target(self):
	"""
	Determines if the goalkeeper is at the target location. Returns True/False
	"""
		return math.fabs(self.target.centerx - self.rect.centerx) <= math.fabs(self.velocity)
		


class Vector():
	'''
	Generic vector object to handle direction, position, and speed
	'''
	def __init__(self, x, y):
		self.x = x
		self.y = y

	def __str__(self): 
	# printing vectors in console
		return "(%s, %s)"%(self.x, self.y)

	def __getitem__(self, key):
		if key == 0:
			return self.x
		elif key == 1:
			return self.y
		else:
			raise IndexError(str(key)+" is not a vector key")

	def __sub__(self, o): 
	# vector subtraction
		return Vector(self.x - o.x, self.y - o.y)

	def length(self): 
	# get vector length
		return math.sqrt((self.x**2 + self.y**2)) 

	def normalize(self): 
	# normalize vector by length
		l = self.length()
		if l != 0:
			return (self.x / l, self.y / l)
		return None
