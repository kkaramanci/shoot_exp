####### shoot_once_prepare ##############################################################
# This section initializes objects, configurations and graphics during the preperation of the experiment.
# Occurs before running a trial so that processing times do not affect experimental timing
# 
# Initializes the main Game singleton object, as well as the individual graphic assets
# Assigns initial positions to all assets on screen, as a function relative to screen size
# The main run routine is also declared, which updates the asset positions, once per frame
# The update() functions of each object is called once per frame
# Subject actions are also defined in the main routine
# 



###### EXPERIMENT PARAMETERS & CONFIGURATION ###################################

# Load experiment parameters
# These are read in from trial block loop spreadsheets in the OpenSesame experiments
# may vary per trial or be fixed throughout experiment as setup in the experiment
i_speed = self.get('i_speed')
i_difficulty = self.get('i_difficulty')
i_agency = 1
i_shoot_speed = self.get('i_shoot_speed')
i_deviation = self.get("i_deviation")

# configurations to define default screen size
#(should be small enough to ensure compatibility across screens)
# configurations to locate settings dictionaries as declared in the entities_run ile
configuration = {
		'screen_size': (640,640),
		'speed': SPEED[i_speed],
		'difficulty': DIFFICULTY[i_difficulty],
		'shoot_speed': SHOOT_SPEED[i_shoot_speed],
		'agency': 1,
		'deviation': i_deviation,
		'bar_size':200,
		'sound_nogoal': path + 'nogoal.wav',
		'sound_goal': path + 'goal.wav',
		'sound': True
	}
	
class Game(object):
"""
Main singleton Game class which contains the entire experiment setting and graphics
"""
	def __init__(self, configuration, input_state):
	"""
	Initialize all settings and the initial graphics environment. Indicate the locations of
	each of the graphics assets
	"""
		# initialize game clock
		self.game_clock = pygame.time.Clock()

		
		# initialize game states        
		self.shooting = False
		self.scored = False
		self.not_scored = False
		self.cleanup = False
		self.correct = None
		
		self.configuration = configuration
		self.input_state = input_state
		# load background image
		self.background = pygame.image.load(path + "bg.jpg").convert()
		# initialize all sprites (game graphics assets which update once per frame)
		self.sprites = pygame.sprite.OrderedUpdates()
		# Position the goal object and add it to the Game sprites list
		self.goal = Goal((configuration['screen_size'][0]/4,
								 configuration['screen_size'][1]/3), 
								 configuration['agency'],
								 configuration['deviation'],
								 self.sprites)
		# Position the feedback object and add it to the Game sprites list
		self.feedback = Feedback((configuration['screen_size'][0]*7/18,
								 configuration['screen_size'][1]*2/10), self.sprites)
		# Position the ball object and add it to the Game sprites list				 
		self.ball = Ball((self.goal.rect.centerx,
								 configuration['screen_size'][1]*3/4), self.sprites)
		self.ball.rect.centerx = self.goal.rect.centerx
		
		self.agency = configuration['agency']
		self.sound_nogoal = pygame.mixer.Sound(configuration['sound_nogoal'])
		self.sound_goal = pygame.mixer.Sound(configuration['sound_goal'])
		self.reset_game(random.random()<0.5)
		self.running = True
		
		
	def play_sound(self, sound):
	"""
	Function that plays a sound
	"""
		if self.configuration['sound']:
			sound.play()
		
	def reset_game(self, serveLeft=True):
		pass
	
	def update(self):
	"""
	Main update function for the game. Runs once per frame.
	Triggers all other object update functions.
	"""
		# If game is in cleanup state
		if self.cleanup:
			self.running = False
			self.shooting = False
			self.scored = False
			self.not_scored = False
			self.cleanup = False
			self.feedback.hideText()
			#self.slider.reset()
			#self.ball.reset()
			#self.goal.paint_target(self.slider.get_pos())
			
		# If the game is in a shooting state, we will check for when it arrives at target.
		# Game will be reset 1s after arriving at target at the conclusion of the trial.
		elif self.shooting:
			if (self.ball.in_target()):
				self.shooting = False
				now = pygame.time.get_ticks()            
				self.release_time = now + 1000            

		# If the game is in a scored state, the appropriate feedback is shown and correct states are set
		# Once the 1s timer expires (which was set at the end of shooting state), the trial is reset
		elif self.scored:
			self.feedback.showText("GOL!!!", green)
			#self.sound_goal.play()
			if(pygame.time.get_ticks() > self.release_time):
				self.scored = False
				self.running = False
				self.cleanup = True
			
		# If the game is in a not scored state, the appropriate feedback is shown and correct states are set
		# Once the 1s timer expires (which was set at the end of shooting state), the trial is reset
		elif self.not_scored:
			self.feedback.showText("KURTARDI", red)
			#self.sound_nogoal.play()
			if(pygame.time.get_ticks() > self.release_time):
				self.not_scored = False
				self.cleanup = True
		
		# If a keyboard input is entered and is one of the correct inputs, game enters "shooting" state
		elif self.input_state['key'] == pygame.K_a or self.input_state['key'] == pygame.K_d:
			#now = pygame.time.get_ticks()            
			#self.release_time = now + 1500            
			
			self.shooting = True
			
			#check to see if goalkeeper position allows for a save, given the keyboard input
			saved = self.saved()

			# If not saved, set the appropriate states
			if not saved:
				self.goal.goalkeeper.saving = True
				self.ball.shoot(self.goal.target.rect)
				self.scored = True
				self.correct = True
				self.goal.goalkeeper.move(pygame.Rect(self.goal.rect.centerx, self.goal.rect.centery,1,1))
				
			# If saved, set the appropriate states and set the target of the goalkeeper to match ball trajectory
			else:
				self.goal.goalkeeper.saving = True
				self.ball.shoot(self.goal.target.rect)
				self.goal.goalkeeper.move(self.goal.target.rect)
				self.not_scored = True
				self.correct = False
					
		
		# Trigger update functions of each object contained in the Game object. Occurs once per frame
		self.sprites.update()

	def draw(self, display_surface):
	"""
	This function draws all graphics onto the game screen in given positions and settings
	"""
		self.sprites.clear(display_surface, self.background)
		return self.sprites.draw(display_surface)
	
	def saved(self):
	"""
	Checks if the input keyboard action is the right one, given the deviation of the goalkeeper from the center
	Returns True/False
	"""
		if self.input_state['key'] == pygame.K_a:
			self.goal.paint_target([self.goal.rect.x+25, 0])
			return self.goal.goalkeeper.center < self.goal.rect.centerx
		else:
			self.goal.paint_target([self.goal.rect.x+self.goal.rect.width - 50, 0])
			return self.goal.goalkeeper.center > self.goal.rect.centerx
				
	def is_correct(self):
	"""
	Returns true if the keyboard action was the correct one for the trial
	"""
		return self.correct	
		

# initialize pygame module
pygame.init()
# initialize global clock
clock = pygame.time.Clock()

# initialize color RGB values
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
darkBlue = (0,0,128)
white = (255,255,255)
black = (0,0,0)

# initialize display surface global variables and load background image
display_surface = win
output_surface = display_surface.copy()
output_surface = pygame.image.load(path + "bg.jpg").convert()

# initialize keyboard input state
input_state = {'key': None}
# create game singleton object
game = Game(configuration, input_state)

# Main routine that runs the experiment. Is called once per trial
def run():
	# initialize response time and t1 to help calculate response time
	rt = 0
	t1 = 0
	timestamp = 1
	
	#runs forever while game has not exited or trial not concluded
	while game.running:
		# Ensures that game runs at 60 frames per second (fps). Delays if faster.
		clock.tick_busy_loop(60)
		
		#Calculate and display fps every 2 seconds
		now = pygame.time.get_ticks()
		if timestamp > 0 and timestamp < now:
			timestamp = now + 2000
			print clock.get_fps()
			
		# Handler for game quit and keyboard press events
		for event in pygame.event.get():
			# quit game if quit event triggered
			if event.type == pygame.QUIT:
				game.running = False
			# trigger quit event if escape key is pressed
			elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
				game.running = False
				exp.set('exp_exit_signal', 1)
			# ignore key press event if pressed keys are not relevant to experiment
			elif event.type == pygame.KEYDOWN and not(event.key == pygame.K_a or event.key == pygame.K_d):
				pass
			# record the key that was pressed if relevant to the experiment. Calculate and record response time
			elif event.type == pygame.KEYDOWN and not(input_state.get('key')):
				rt = self.time() - t1
				input_state['key'] = event.key

		
		# call update functions (occurs once per frame)
		game.update()			
		display_surface.fill(black)
		
		# draw graphics (or redraw altered graphics) in the second (hidden) screen
		game.draw(output_surface)
		display_surface.blit(output_surface, (0,0))
		# present the hidden screen
		pygame.display.flip()
		# set the start time of the experiment, if not set. Used to calculate rt
		if (t1 == 0):
			t1 = self.time()
	
	# at the conclusion of the experiment, return the response time and result
	return rt, game.is_correct()
