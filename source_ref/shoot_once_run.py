####### shoot_once_prepare ##############################################################
# This section runs the trial. Assets were previously initialized in preparation. 
# Has minimal code to ensure timing accuracy. 
# 
# 

# Set previous trial agency. Used in agency experiments.
exp.set('prev_agency', i_agency)

# Run the main trial function. Response time and accuracy are returned.
rt, cor = run()

# Response keypress, response time and accuracy are recorded
exp.set_response(response=input_state['key'], response_time=rt,correct=cor),
