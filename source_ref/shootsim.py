"""
Likelihood Divergence Simulation of Samples for Shoot Task

This simulation samples from a normal distribution that matches the experiment
task distribution, deviated either to the right or left of center. 
It then plots the divergence of likelihood ratios for the pdf of left
and right deviated normal distributions, as more samples are obtained.
"""

import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt



# mean and std for normal distributions match the experimental distributions
# for each of the easy, medium and hard settings of the experiment
meanL = [240,294,312]
stdL = [80, 80, 80]
meanR = [400,346,328]
stdR = [80, 80, 80]
taskLabel = ['Easy','Medium','Hard']
taskOrder = [0, 1, 2]

# number of samples and experimental runs to simulate
sampleCount = 100
roundCount = 10



# for each of easy, medium and hard tasks
for task in taskOrder:

# setup the normal distribution variables
    normL = st.norm(meanL[task], stdL[task])
    normR = st.norm(meanR[task], stdR[task])

# Generate 10 rounds at random to left or right of goal. 0 for L, 1 for R

    LorR = np.random.randint(2, size=roundCount) 

# For each shooting round (with goalie to the L or R)
    for pick in LorR:
        
# Generate values for normal distribution to the left and right of goal
        simL = normL.rvs(sampleCount)
        simR = normR.rvs(sampleCount)  
        
        if pick:          #1 is R, 0 is L
        
#plot the cumulative sum of likelihood ratios for L and R normal  distributions
#likelihood accumulation resembles evidence accumulation model
            likeDistL = normL.logpdf(simR)
            likeDistR = normR.logpdf(simR)
            plt.plot(np.cumsum(likeDistR - likeDistL), color='blue')
        else:
            likeDistL = normL.logpdf(simL)
            likeDistR = normR.logpdf(simL)
            plt.plot(np.cumsum(likeDistR - likeDistL), color='red')
            
#Plot for each of easy, medium and hard tasks            
    plt.xlabel('Samples')
    plt.ylabel('Sum Likelihood')
    plt.title(taskLabel[task] + ' Task - Likelihood accumulation')
    plt.savefig(taskLabel[task] + '.png')    
    plt.show()


